import { NgModule } 		from '@angular/core';
import { BrowserModule }  	from '@angular/platform-browser';
import {NgbModule}    		from '@ng-bootstrap/ng-bootstrap';
import { RouterModule }   from '@angular/router';

import { AppComponent } 	from './app.component';
import { LayoutComponent }   from '../layout/layout.component';
import { MenuComponent }   from '../menu/menu.component';
import { HomeComponent }   from '../home/home.component';
import { ListComponent }   from '../list/list.component';

@NgModule({
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path:'',
        component : HomeComponent
      },{
        path:'list',
        component : ListComponent
      }
    ])
  ],
  declarations: [
    AppComponent,
    LayoutComponent,
    MenuComponent,
    HomeComponent,
    ListComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
